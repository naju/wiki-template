base     white_mod
author   Menzel, Lukas; Lee, Kwangyoung
email    lukas.menzel@naju.de
date     2019-06-07
name     White Template Mod
desc     Simple, minimal and responsive template.
url      https://www.dokuwiki.org/template:white
