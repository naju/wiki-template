<?php
/**
 * DokuWiki Starter Template
 *
 * @link     http://dokuwiki.org/template:ipari
 * @author   Kwangyoung Lee <ipari@leaflette.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die();
@require_once(dirname(__FILE__).'/tpl_functions.php');
header('X-UA-Compatible: IE=edge,chrome=1');
$showSidebar = page_findnearest($conf['sidebar']);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php echo strip_tags($conf['title']) ?> – <?php tpl_pagetitle() ?></title>
    <script>
        (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement);
        (function() {
            // critical js for async icon file loading. done this way to improve caching
            var client = new XMLHttpRequest()
            client.open("GET", "<?php print tpl_basedir(); ?>images/dripicons.svg")
            client.onloadend = function() {
                var tpl = document.createElement('template')
                var html = client.responseText.trim()
                tpl.innerHTML = html
                document.head.append(tpl.content.firstChild)
            }
            client.send()
        })();
    </script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
</head>

<body id="dokuwiki__top">
    <div id="dokuwiki__site" class="<?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">
        <?php html_msgarea() ?>
        <?php tpl_includeFile('header.html') ?>

        <!-- ********** HEADER ********** -->
        <header id="dokuwiki__header">
            <div class="group">
                <h1><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                <div class="left">
                    <?php if ($showSidebar): ?>
                    <button class="btn_left" accesskey="s", title="Navigation">
                        <?php white_mod_icon('map', 'Navigation'); ?>
                    </button>
                    <?php endif; ?>
                </div>
                <div class="right">
                    <button class="btn_search">
                        <?php white_mod_icon('search', $lang['btn_search']); ?>
                    </button>
                    <button class="btn_right" accesskey="m" title="Tools">
                        <?php white_mod_icon('menu', 'Menu'); ?>
                    </button>
                </div>
            </div>
            <div class="search">
                <?php tpl_searchform(); ?>
            </div>
        </header><!-- /header -->

        <!-- ********** sidebar ********** -->
        <aside id="sidebar_wrapper">
            <!-- ********** ASIDE ********** -->
            <?php if ($showSidebar): ?>
            <div id="dokuwiki__aside"  class="sidebar hidden">
                <?php tpl_includeFile('sidebarheader.html') ?>
                <?php tpl_include_page($conf['sidebar'], 1, 1) ?>
                <?php tpl_includeFile('sidebarfooter.html') ?>
            </div><!-- /dokuwiki__aside -->
            <?php endif; ?>

            <div id="dokuwiki__tools" class="sidebar left hidden">
                <!-- PAGE TOOLS -->
                <div id="dokuwiki__pagetools">
                    <h3><?php echo $lang['page_tools'] ?></h3>
                    <ul><?php
                        $page_actions = array(
                            'edit'      => tpl_action(
                                'edit', true, 'li', true,
                                white_mod_icon_ret('document-edit').'<span>', '</span>'
                            ),
                            'revisions' => tpl_action(
                                'revisions', true, 'li', true,
                                white_mod_icon_ret('archive').'<span>', '</span>'
                            ),
                            'backlink'  => tpl_action(
                                'backlink', true, 'li', true,
                                white_mod_icon_ret('link').'<span>', '</span>'
                            ),
                            'subscribe' => tpl_action(
                                'subscribe', true, 'li', true,
                                white_mod_icon_ret('feed').'<span>', '</span>'
                            ),
                            'revert'    => tpl_action(
                                'revert', true, 'li', true,
                                white_mod_icon_ret('backspace').'<span>', '</span>'
                            )
                        );

                        if (!plugin_isdisabled('odt')) {
                            $params = array('do' => 'export_odt');
                            if($REV) { $params['rev'] = $REV; }
                            $page_actions['export_odt_button'] = '<li>
                                <a rel="nofollow"
                                    href="'.wl($ID, $params).'"
                                    title="'.tpl_getLang('export_odt_button').'
                                ">
                                    '.white_mod_icon_ret('document').
                                    '<span>'.tpl_getLang('export_odt_button').'</span>
                                </a>
                            </li>';
                        }

                        white_toolsevent('pagetools', $page_actions);
                    ?></ul>
                </div><!-- /dokuwiki__pagetools -->

                <!-- SITE TOOLS -->
                <div id="dokuwiki__sitetools">
                    <h3><?php echo $lang['site_tools'] ?></h3>
                    <ul>
                        <?php white_toolsevent('sitetools', array(
                            'recent'    => tpl_action(
                                'recent', true, 'li', true,
                                white_mod_icon_ret('clock').'<span>', '</span>'
                            ),
                            'media'     => tpl_action(
                                'media', true, 'li', true,
                                white_mod_icon_ret('photo').'<span>', '</span>'
                            ),
                            'index'     => tpl_action(
                                'index', true, 'li', true,
                                white_mod_icon_ret('map').'<span>', '</span>'
                            ),
                        )); ?>
                    </ul>
                </div><!-- /dokuwiki__sitetools -->

                <!-- USER TOOLS -->
                <?php if ($conf['useacl']): ?>
                <div id="dokuwiki__usertools">
                    <h3><?php // TODO: this should be implemented as the old way is deprecated.
                    /* echo $lang['user_tools'] ?></h3>
                    <?php echo (new \dokuwiki\Menu\UserMenu())->getListItems(); */ ?>
                    <h3><?php echo $lang['user_tools'] ?></h3>
                    <ul>
                        <?php white_toolsevent('usertools', array(
                            'admin'     => tpl_action(
                                'admin', true, 'li', true,
                                white_mod_icon_ret('gear').'<span>', '</span>'
                            ),
                            'profile'   => tpl_action(
                                'profile', true, 'li', true,
                                white_mod_icon_ret('user').'<span>', '</span>'
                            ),
                            'register'  => tpl_action(
                                'register', true, 'li', true,
                                white_mod_icon_ret('checkmark').'<span>', '</span>'
                            ),
                            'login'     => tpl_action(
                                'login', true, 'li', true,
                                white_mod_icon_ret('enter').'<span>', '</span>'
                            ),
                        )); ?>
                    </ul>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<div class="user">';
                            tpl_userinfo();
                            echo '</div>';
                        }
                    ?>
                </div><!-- /dokuwiki__usertools -->
                <?php endif ?>
            </div><!-- /dokuwiki__tools -->

            <div id="sidebar_bg">
            </div>

            <div id="to_top">
                <?php tpl_action('top') ?>
            </div>
        </aside><!-- /sidebar_wrapper -->

        <div class="wrapper group">
            <!-- ********** CONTENT ********** -->
            <div id="dokuwiki__content"><div class="group">
                <?php tpl_flush() ?>
                <?php tpl_includeFile('pageheader.html') ?>

                <!-- BREADCRUMBS -->
                <?php if($conf['breadcrumbs']){ ?>
                    <div class="breadcrumbs"><?php tpl_breadcrumbs($ret='›') ?></div>
                <?php } ?>
                <?php if($conf['youarehere']){ ?>
                    <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                <?php } ?>

                <div class="page group
                <?php if(tpl_getConf('numberedHeading')): ?> numbered_heading<?php endif ?>
                <?php if(tpl_getConf('tocPosition')): ?> toc_<?php echo tpl_getConf('tocPosition') ?><?php endif ?>
                ">
                    <!-- wikipage start -->
                    <?php tpl_content() ?>
                    <!-- wikipage stop -->
                </div>

                <?php tpl_flush() ?>
                <?php tpl_includeFile('pagefooter.html') ?>
            </div></div><!-- /content -->

            <!-- ********** FOOTER ********** -->
            <div id="dokuwiki__footer">
                <?php if($INFO['exists']): ?>
                <div class="doc"><?php white_pageinfo() ?></div>
                <?php endif ?>
                <?php tpl_includeFile('sidebarfooter.html') ?>
                <?php tpl_license('badge', false, false) ?>
                <div class="footer">
                <?php tpl_include_page(tpl_getConf('footer'), 1, 1) ?>
                </div>
            </div><!-- /footer -->

            <?php tpl_includeFile('footer.html') ?>
        </div><!-- /wrapper -->

    </div><!-- /site -->

    <div class="no"><?php
        tpl_indexerWebBug();
        /* provide DokuWiki housekeeping, required in all templates */
    ?></div>
    <script>
    window.addEventListener('load', function() {
        var $ = jQuery;
        <?php if (!plugin_isdisabled('discussion')) { ?>
        var searchParams = new URLSearchParams(window.location.search);
        var btn_vals = [
            '<?php print tpl_getLang("discussion_show"); ?>',
            '<?php print tpl_getLang("discussion_hide"); ?>'
        ];

        // only hide comments if we are not editing them
        if (
          searchParams.has('comment')
          || window.location.hash.indexOf('#comment_') === 0
          || window.location.pathname.indexOf('feedback') >= 0
        ) {
            $('#discussion__btn_toggle_visibility').val(btn_vals[1]);
        } else {
            $('#discussion__btn_toggle_visibility').val(btn_vals[0]);
            $('#comment_wrapper').toggle();
        }
        $('#discussion__btn_toggle_visibility').click(function() {
            $(this).val(($(this).val() == btn_vals[0]) ? btn_vals[1]: btn_vals[0]);
        });
        <?php } ?>
    });
    </script>
</body>
</html>
