(function($) {
    var fadeOption = {duration: 150};
    var sidebarState = {
        aside: false,
        tools: false
    };

    function hideSidebar(selector) {
        $(selector).addClass('hidden');
        $(selector).on('transitionend', function() {
            $(this).hide(0);
            $(this).off('transitionend');
        });
    }

    function showSidebar(selector) {
        $(selector).show('fade', {duration:0}, function(){ $(this).removeClass('hidden') });
    }

    function applyState() {
        if ((sidebarState.tools || sidebarState.aside) && $('#sidebar_bg').is(':hidden'))
            $('#sidebar_bg').show('fade', fadeOption);
        if (!sidebarState.aside && !sidebarState.tools)
            $('#sidebar_bg').hide('fade', fadeOption);

        if (sidebarState.aside && $('#dokuwiki__aside').is(':hidden'))
            showSidebar('#dokuwiki__aside');
        if (!sidebarState.aside && $('#dokuwiki__aside').is(':visible'))
            hideSidebar('#dokuwiki__aside');

        if (sidebarState.tools && $('#dokuwiki__tools').is(':hidden'))
            showSidebar('#dokuwiki__tools');
        if (!sidebarState.tools && $('#dokuwiki__tools').is(':visible'))
            hideSidebar('#dokuwiki__tools');
    }

    function preventParentWheel(e) {
    	  var curScrollPos = $(this).scrollTop();
    	  var scrollableDist = $(this).prop('scrollHeight') - $(this).outerHeight();
    	  var wheelEvent = e.originalEvent;
    	  var dY = wheelEvent.deltaY;

    	  if (dY < 0 && curScrollPos <= 0) {
    	  	  return false;
    	  }
    	  if (dY > 0 && curScrollPos >= scrollableDist) {
    	  	  return false;
    	  }
    }

    function showSearch() {
        $('div.search').toggle();
        $('div.search').find('input.edit').select();
    }

    function bindEvents() {
        $('.sidebar').on('wheel scroll', preventParentWheel);
        $('.btn_left').click(function() {
            if ($(window).width() < 640)
                sidebarState.tools = false;
            sidebarState.aside = !sidebarState.aside;
            applyState();
        });
        $('.btn_right').click(function() {
            if($(window).width() < 640)
                sidebarState.aside = false;
            sidebarState.tools = !sidebarState.tools;
            applyState();
        });
        $('#sidebar_bg').click(function() {
            sidebarState.aside = false;
            sidebarState.tools = false;
            applyState();
        });
        $('.btn_search').click(function() {
            showSearch();
        });
        $(document).keydown(function(e) {
            if (e.which == 70 && e.altKey) {
                showSearch();
                e.preventDefault();
            }
        });

    }

    function initUI() {
        // fade out success message after 5 sec
        setTimeout(function() {
          $('div.success, div.error').hide('fade', fadeOption);
        }, 5000);

        // Move TOC
        if ($('.page h2').length > 0) {
            $('.toc_wikipedia').find('#dw__toc').insertBefore($('.page h2:first'));
        } else {
            $('.toc_wikipedia').find('#dw__toc').insertAfter($('.page h1:first').next('.level1'));
        }
        $('.toc_dokuwiki').find('#dw__toc').insertAfter($('.page h1:first'));

        // Anchor link should be shifted by header pixel
        // $(window).on("hashchange", function (e) {
        //     e.preventDefault();
        //     window.scrollTo(window.scrollX, window.scrollY - 48);
        // });
    }

    $(function() {
        initUI();
        bindEvents();
    });
})(jQuery);
